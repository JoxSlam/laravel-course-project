<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Post;

class EditPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EditPost:edit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Edit a post';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $id = $this->ask('Enter the post ID');
        $post = Post::find($id);
        if ($post) {
            $this->show($post);
        } else {
            $this->info('Post not found');
            return;
        }

        $this->info('Leave empty if you don\'t want to change the value');
        $title_en = $this->ask('Enter the English title');
        $title_id = $this->ask('Enter the Indonesian title');
        $content_en = $this->ask('Enter the English content');
        $content_id = $this->ask('Enter the Indonesian content');
        $save = $this->confirm('Do you want to save the changes?');

        if ($save && $title_en && $title_id && $content_en && $content_id) {
            $this->edit($id, $title_en, $title_id, $content_en, $content_id);
            $this->info('Post updated successfully');
        } else {
            $this->info('Post not updated');
        }
    }

    public function edit(string $id, string $title_en, string $title_id, string $content_en, string $content_id)
    {
        $post = Post::find($id);
        $post->title_en = $title_en;
        $post->title_id = $title_id;
        $post->content_en = $content_en;
        $post->content_id = $content_id;
        $post->save();
        $this->show($post);
    }

    public function show(Post $post)
    {
        $this->info('ID: ' . $post->id);
        $this->info('English title: ' . $post->title_en);
        $this->info('Indonesian title: ' . $post->title_id);
        $this->info('English content: ' . $post->content_en);
        $this->info('Indonesian content: ' . $post->content_id);
    }
}
