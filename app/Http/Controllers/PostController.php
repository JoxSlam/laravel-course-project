<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $keyCache = "posts";
        $posts = Cache::get($keyCache);
        if (!$posts) {
            $posts = Post::with('user')->get()->sortByDesc('created_at');
            Cache::put($keyCache, $posts, 2);
        }
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('posts.create', ['token' => csrf_token()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostCreateRequest $request)
    {
        $data = $request->validated();
        $user = Auth::user();
        $post = new Post($data);
        $post->user_id = $user->id;
        $post->save();
        return redirect()->route('post.index')->with('notification', 'Post created successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = Auth::user();
        $post = Post::where("user_id", $user->id)->where("id", $id)->first();
        if (!$post) {
            return redirect()->route('post.index')->with('error', 'Post not found!');
        }
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PostUpdateRequest $request, string $id)
    {
        $data = $request->validated();
        $user = Auth::user();
        $post = Post::where("user_id", $user->id)->where("id", $id)->first();
        if (!$post) {
            return redirect('/posts')->with('error', 'Post not found!');
        }
        $post->fill($data);
        $post->save();

        return redirect('/posts')->with('notification', 'Post updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = Auth::user();
        $post = Post::where("user_id", $user->id)->where("id", $id)->first();
        if (!$post) {
            return redirect('/posts')->with('error', 'Post not found!');
        }
        $post->delete();
        return redirect('/posts')->with('notification', 'Post deleted successfully!');
    }
}
