<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserLoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(UserLoginRequest $request)
    {
        $data = $request->validated();
        $user = User::where("email", $data["email"])->first();
        if ($user && Auth::attempt($data)) {
            $user->token = Str::uuid()->toString();
            $user->save();
            $cookie = Cookie::make('token', $user->token, 60 * 24 * 30);
            return redirect('/posts')->with("success", 'Logged in successfully! Welcome ' . $user->name)->withCookie($cookie);
        }
        return redirect()->back()->withInput()->withErrors(["Auth" => "Invalid email or password"]);
    }

    public function logout()
    {
        $user = Auth::user();
        $user->token = null;
        $user->save();
        $cookie = Cookie::forget('token');
        Auth::logout();
        return redirect()->route('view.login')->withCookie($cookie)->with("success", "Logged out successfully!");
    }
}
