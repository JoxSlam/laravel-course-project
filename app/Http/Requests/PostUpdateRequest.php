<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class PostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->user() !== NULL;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title_en' => 'required|string|max:255',
            'title_id' => 'required|string|max:255',
            'content_en' => 'required|string',
            'content_id' => 'required|string',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        return redirect()->back()->withInput()->withErrors($validator->errors());
    }
}
