<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Cookie;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $token = Cookie::get('token');
        $authenticate = TRUE;

        if (!$token) {
            $authenticate = FALSE;
        }

        $user = User::where('token', $token)->first();
        if (!$user) {
            $authenticate = FALSE;
        } else {
            Auth::login($user);
        }

        if ($authenticate) {
            return $next($request);
        } else {
            return redirect()->route('view.login')->withErrors(['Auth' => 'Please login first']);
        }
    }
}
