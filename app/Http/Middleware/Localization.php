<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $languages = 'en';

        if (request('languages')) {
            $languages = request('languages');
            session()->put('languages', $languages);
        } elseif (session('languages')) {
            $languages = session('languages');
        }
        app()->setLocale($languages);

        return $next($request);
    }
}
