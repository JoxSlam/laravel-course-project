<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="bg-slate-800">
    <nav class="bg-slate-900 p-2">
        <div class="container mx-auto flex justify-between items-center">
            <a href="" class="text-white text-lg font-bold">Laravel 10 Task App</a>
            <div>
                @foreach (config('app.available_locales') as $locale)
                    <a href="{{ request()->fullUrlWithQuery(['languages' => $locale]) }}"
                        class="text-white hover:text-gray-400 px-4 {{ app()->getLocale() === $locale ? 'underline' : '' }}">
                        {{ strtoupper($locale) }}
                    </a>
                @endforeach
            </div>
        </div>
    </nav>
    {{ $slot }}
</body>

</html>
