<x-layout>
    <main class="flex flex-col justify-center w-full mt-[100px] space-y-2">
        <h1 class="text-2xl font-bold text-center text-white">{{ __('Login') }}</h1>
        <div class="bg-slate-300 w-[400px] mx-auto p-5 rounded-lg">
            @if (session()->has('errors'))
                <div class="bg-red-500 text-white p-2 rounded">
                    <ul class="space-y-2">
                        @foreach (session('errors')->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('success'))
                <div class="bg-green-500 text-white p-2 rounded">
                    {{ session('success') }}
                </div>
            @endif
            <form action="{{ route('auth.login') }}" method="post" class="space-y-5">
                <div class="flex flex-col">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="border-2 border-slate-500 rounded"
                        value="{{ old('email') }}">
                    @error('email')
                        <span class="text-red-500">{{ $message }}</span>
                    @enderror
                </div>
                <div class="flex flex-col">
                    <label for="password">{{ __('Password') }}</label>
                    <input type="password" name="password" id="password" class="border-2 border-slate-500 rounded">
                </div>
                @csrf
                <p class="text-sm">{{ __('Don\'t have an account?') }}
                    <a href="" class="text-blue-500">{{ __('Register') }}</a>
                </p>
                <div class="flex flex-col">
                    <button type="submit" class="bg-sky-800 text-white p-2 rounded">{{ __('Login') }}</button>
                </div>
            </form>
        </div>
    </main>
</x-layout>
