<x-layout>
    <main>
        <h1 class="text-center font-bold text-white text-2xl mt-5">{{ __('Create New Post') }}</h1>
        <div class="flex justify-between max-w-lg mx-auto">
            <a href="/posts">
                <button class="bg-red-500 text-white py-1 px-2 rounded">Back</button>
            </a>

        </div>

        <div class="max-w-lg mx-auto mt-5">
            <form action="{{ route('post.store') }}" method="post" class="bg-slate-300 p-3 rounded-lg">
                @csrf
                <div class="mb-3">
                    <label for="title" class="block text-sm font-semibold">Title (English)</label>
                    <input type="text" name="title_en" id="title_en"
                        class="w-full p-2 rounded border border-gray-300" value="{{ old('title_en') }}">
                    @error('title_en')
                        <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="content" class="block text-sm font-semibold">Content (English)</label>
                    <textarea name="content_en" id="content_en" class="w-full p-2 rounded border border-gray-300">{{ old('content_en') }}</textarea>
                    @error('content_en')
                        <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="title" class="block text-sm font-semibold">Title (Indonesia)</label>
                    <input type="text" name="title_id" id="title_id"
                        class="w-full p-2 rounded border border-gray-300" value="{{ old('title_id') }}">
                    @error('title_id')
                        <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="content" class="block text-sm font-semibold">Content (Indonesia)</label>
                    <textarea name="content_id" id="content_id" class="w-full p-2 rounded border border-gray-300">{{ old('content_id') }}</textarea>
                    @error('content_id')
                        <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>
                <button type="submit" class="bg-sky-800 text-white py-1 px-2 rounded">Create Post</button>
            </form>
        </div>
    </main>
</x-layout>
