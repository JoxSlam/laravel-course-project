<x-layout>
    <main>
        <h1 class="text-center font-bold text-white text-2xl mt-5">Posts {{app()->getLocale()}}</h1>
        <div class="flex justify-between max-w-lg mx-auto">
            <a href="/posts/create">
            <button class="bg-sky-800 text-white py-1 px-2 rounded">{{ __('Create Post') }}</button>
            </a>
            <a href="/logout">
                <button class="bg-red-500 text-white py-1 px-2 rounded">{{ __('Logout') }}</button>
            </a>
        </div>

        <div class="max-w-lg mx-auto mt-5">
            @if (session()->has('success'))
                <div class="bg-green-500 text-white p-2 rounded mb-3">
                    {{ session('success') }}
                </div>
            @endif
            @if (session()->has('error'))
                <div class="bg-red-500 text-white p-2 rounded mb-3">
                    {{ session('error') }}
                </div>
            @endif
            @foreach ($posts as $post)
                @php
                    $locale = app()->getLocale();
                    $title = "title_$locale";
                    $content = "content_$locale";
                @endphp
                <div class="bg-slate-300 p-3 rounded-lg mb-3">
                    <h2 class="text-xl font-bold">{{ $post->$title }}</h2>
                    <h4 class="text-sm font-semibold">{{ __('Author') }}: {{ $post->user->name }}</h4>
                    <p class="text-sm">{{ $post->$content }}</p>
                    <div class="flex justify-between mt-2">
                        <a href="/posts/{{ $post->id }}/edit">
                            <button class="bg-sky-800 text-white py-1 px-2 rounded">{{ __('Edit') }}</button>
                        </a>
                        <form action="/posts/{{ $post->id }}/delete" method="post">
                            @csrf
                            <button type="submit" class="bg-red-500 text-white py-1 px-2 rounded">{{ __('Delete') }}</button>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
    </main>
    {{-- Modal Notification --}}
    @if (session()->has('notification'))
        <div class="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-slate-800/[0.4]"
            id="modal">
            <div class="absolute w-1/2 bg-white p-3 rounded-lg" id="modal-content">
                <div id="modal-body">
                    {{ session('notification') }}
                </div>
                <div class="flex justify-end mt-3">
                    <button class="bg-red-500 text-white py-1 px-2 rounded" id="modal-close">{{ __('Close') }}</button>
                </div>
            </div>
        </div>
        <script>
            const modalClose = document.getElementById('modal-close');
            modalClose.addEventListener('click', () => {
                modal.style.display = 'none';
            });
        </script>
    @endif
</x-layout>
